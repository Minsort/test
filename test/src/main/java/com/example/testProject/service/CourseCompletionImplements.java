package com.example.testProject.service;

import com.example.testProject.repository.CourseCompletionRepository;

public class CourseCompletionImplements {


final
CourseCompletionRepository courseCompletionRepository;

    public CourseCompletionImplements(CourseCompletionRepository courseCompletionRepository) {
        this.courseCompletionRepository = courseCompletionRepository;
    }

    //Получаем текущий средний балл
    public String avgScores (){
String s = courseCompletionRepository.avgScores();
return s;
    }

    //Финальная отметка.        (Надеюсь, ко мне не будут так требовательны, как я в своих расчетах:)))
    public Integer endScores (){
        return Integer.parseInt(courseCompletionRepository.avgScores());
    }
}
