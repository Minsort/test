package com.example.testProject.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "students")
public class Students {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String address;
    private String numberPhone;
    private String email;
    private Integer recordBook; //номер зачетки
    private float studentPerformance; //Средняя успеваемость

    @OneToMany
    @JoinColumn(name="score")
    public List<CourseCompletion> scoresList=new ArrayList<CourseCompletion>();

public void list(){
    System.out.println(scoresList.toString());
}

}
