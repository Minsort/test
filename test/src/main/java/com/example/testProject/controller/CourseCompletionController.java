package com.example.testProject.controller;


import com.example.testProject.repository.CourseCompletionRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/score")
public class CourseCompletionController {

    final
    CourseCompletionRepository courseCompletionRepository;

    public CourseCompletionController(CourseCompletionRepository courseCompletionRepository) {
        this.courseCompletionRepository = courseCompletionRepository;
    }

    @GetMapping("/avg")
    public String avg (){
       return courseCompletionRepository.avgScores();   //Получить текущий средний балл
    }


    @GetMapping("/end")                                 //Финальная отметка
    public Integer end (){
        return courseCompletionRepository.endScores();
    }















}
