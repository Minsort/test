package com.example.testProject.repository;

import com.example.testProject.models.CourseCompletion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public interface CourseCompletionRepository extends JpaRepository <CourseCompletion, Integer> {


    @Query(value = "SELECT AVG (score) FROM coursecompletion WHERE student_id=20" , nativeQuery = true) ////////
    public String avgScores ();

    @Query(value = "SELECT AVG (score) FROM coursecompletion WHERE student_id=20" , nativeQuery = true) ////////
    public Integer endScores ();


















}
